package com.example.donacionsangre.UI.Main.Solicitud.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SolicitudesResponse(@SerializedName("data")val data:ArrayList<Solicitud>)

data class Solicitud(
    @SerializedName("id") var id: Int,
    @SerializedName("created_at") var createdAt: String,
):Serializable