package com.example.donacionsangre.UI.Main.Solicitud

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.donacionsangre.UI.Main.Solicitud.model.Solicitud
import com.example.donacionsangre.UI.Main.Solicitud.model.SolicitudesResponse
import com.example.donacionsangre.network.Api
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentSolicitudViewModel: ViewModel() {

    val solicitudes = MutableLiveData<ArrayList<Solicitud>>()

    fun getSolicitudes() {
        val response = Api.retrofitService.getSolicitudes()
        response.enqueue(object: Callback<SolicitudesResponse>{

            override fun onResponse(
                call: Call<SolicitudesResponse>,
                response: Response<SolicitudesResponse>
            ) {
                if(response.body()?.data != null) solicitudes.value = response.body()?.data
            }

            override fun onFailure(call: Call<SolicitudesResponse>, t: Throwable) {
                println(t)
            }

        })
    }

}