package com.example.donacionsangre.UI.Main.Mapa


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.donacionsangre.R
import com.example.donacionsangre.adapter.AdapterDatos
import com.example.donacionsangre.data.DataSource
import com.example.donacionsangre.databinding.FragmentMapaBinding

class FragmentMapa : Fragment() {
    private lateinit var viewModel: FragmentMapaViewModel
    companion object {
        fun newInstance() = FragmentMapa()
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentMapaBinding.inflate(inflater,container,false)
        val recyclerView =binding.coordinatorLayout.findViewById<RecyclerView>(R.id.rvLista)
        val myDataset = DataSource().loadAffirmations()
        recyclerView.adapter = AdapterDatos(this, myDataset)
        recyclerView.setHasFixedSize(true)
//        Api.retrofitService.getSolicitudes().enqueue(object: Callback<String> {
//            override fun onResponse(call: Call<String>, response: Response<String>){
//                binding.mensaje.text = response.body()
//            }
//            override fun onFailure(call: Call<String>,t:Throwable){
//                binding.mensaje.text = " "
//            }
//        })
        return binding.root
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(FragmentMapaViewModel::class.java)
    }
}