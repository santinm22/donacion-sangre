package com.example.donacionsangre.UI.Main.Solicitud

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.donacionsangre.R
import com.example.donacionsangre.UI.Main.Certificado.FragmentCertificadoViewModel

class FragmentSolicitud : Fragment() {
    private lateinit var viewModel: FragmentSolicitudViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_solicitud, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(FragmentSolicitudViewModel::class.java)
        viewModel.getSolicitudes()
    }
}