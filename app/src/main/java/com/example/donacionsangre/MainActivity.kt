package com.example.donacionsangre

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.example.donacionsangre.UI.Main.Certificado.FragmentCertificado
import com.example.donacionsangre.UI.Main.Mapa.FragmentMapa
import com.example.donacionsangre.UI.Main.Perfil.FragmentPerfil
import com.example.donacionsangre.UI.Main.Solicitud.FragmentSolicitud
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, FragmentMapa.newInstance())
                .commitNow()
        }
        val navListener =
            BottomNavigationView.OnNavigationItemSelectedListener { item: MenuItem ->

                var selectedFragment: Fragment? = null
                val itemId = item.itemId
                if (itemId == R.id.Mapa) {
                    selectedFragment = FragmentMapa()
                } else if (itemId == R.id.Solicitud) {
                    selectedFragment = FragmentSolicitud()
                } else if (itemId == R.id.Certificado) {
                    selectedFragment = FragmentCertificado()
                } else if (itemId == R.id.Perfil) {
                    selectedFragment = FragmentPerfil()
                }
                if (selectedFragment != null) {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, selectedFragment).commit()
                }
                true
            }
        val bottomNav = findViewById<BottomNavigationView>(R.id.bottom_navigation)
        bottomNav.setOnNavigationItemSelectedListener(navListener)
    }
}